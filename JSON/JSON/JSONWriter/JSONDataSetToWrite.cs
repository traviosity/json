﻿using System.Collections.Generic;
using System.Linq;

namespace JSON
{
    public class JSONDataToWriteInvalidNameException : JSONException
    {
        public JSONDataToWriteInvalidNameException(string msg) : base(msg)
        { }
    }

    public class JSONDataSetToWrite
    {
        public readonly bool isArray;
        public readonly LinkedList<JSONDataToWrite> listOfData;

        public JSONDataSetToWrite(bool isArray = false)
        {
            listOfData = new LinkedList<JSONDataToWrite>();
            this.isArray = isArray;
        }

        public JSONDataSetToWrite AddArray(string name = null)
        {
            return AddDataSetBase(true, name);
        }

        public JSONDataSetToWrite AddDataSet(string name = null)
        {
            return AddDataSetBase(false, name);
        }

        private JSONDataSetToWrite AddDataSetBase(bool isArray, string name = null)
        {
            if (!this.isArray && name == null)
                throw new JSONDataToWriteInvalidNameException("Name of data in not array DataSet must be not null");

            if (!this.isArray && !listOfData.All(data => data.name != name))
                throw new JSONDataToWriteInvalidNameException("Data with name '" + name + "' already exists");

            var newData = JSONDataToWrite.Create(this.isArray ? null : name, new JSONDataSetToWrite(isArray));
            listOfData.AddLast(newData);

            return newData.dataSet;
        }

        public void AddData<T>(T content)
        {
            AddData(null, content);
        }

        public void AddData<T>(string name, T content)
        {
            if (!isArray && name == null)
                throw new JSONDataToWriteInvalidNameException("Name of data in not array DataSet must be not null");

            if (!isArray && !listOfData.All(data => data.name != name))
                throw new JSONDataToWriteInvalidNameException("Data with name '" + name + "' already exists");

            var newData = JSONDataToWrite.Create(isArray ? null : name, content);
            listOfData.AddLast(newData);
        }
    }
}
